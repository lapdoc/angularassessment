import { Component, OnInit } from '@angular/core';
import { Investor } from './models/Investor';
import { InvestorServiceService } from './investor-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-assessment';
  investors:Investor[] = [];
  selectedId:number;
  cash:number;

  constructor(private investorService:InvestorServiceService) {

  }

  ngOnInit() {
  }

  getAllHandler() {
    this.investorService.getInvestors().subscribe((data:Investor[]) => {
      this.investors = [];
      data.forEach((investor) => {
        this.investors.push(investor);
      });
    });
  }

  getByIdHandler() {
    this.investorService.getInvestorById(this.selectedId).subscribe((data:Investor) => {
      this.investors = [];
      this.investors.push(data);
    });
    this.selectedId = null;
  }

  createHandler() {
    this.investorService.createInvestor(this.cash).subscribe(() => {
      this.getAllHandler();
    });
  }

  deleteByIdHandler() {
    this.investorService.deleteInvestor(this.selectedId).subscribe(() => {
      this.getAllHandler();
    });
    this.selectedId = null;
  }

}
