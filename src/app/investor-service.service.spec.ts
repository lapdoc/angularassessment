import { TestBed } from '@angular/core/testing';

import { InvestorServiceService } from './investor-service.service';

describe('InvestorServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvestorServiceService = TestBed.get(InvestorServiceService);
    expect(service).toBeTruthy();
  });
});
