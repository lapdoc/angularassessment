import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Investor } from './models/Investor';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InvestorServiceService {

  investorUrl:string = 'http://owaisyananinvestor.dev1.conygre.com/accounts';

  constructor(private http:HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }

  getInvestors():Observable<Investor[]> {
    return this.http.get<Investor[]>(this.investorUrl)
    .pipe(retry(3), catchError(this.handleError));
  }

  getInvestorById(id:number): Observable<Investor> {
    return this.http.get<Investor>(`${this.investorUrl}/${id}`)
    .pipe(retry(3), catchError(this.handleError));
  }

  createInvestor(cash:number) {
    return this.http.post(`${this.investorUrl}?cash=${cash}`, {}, {})
    .pipe(retry(3), catchError(this.handleError));
  }

  deleteInvestor(id:number) {
    return this.http.delete(`${this.investorUrl}?ID=${id}`, {})
    .pipe(retry(3), catchError(this.handleError));
  }


}
